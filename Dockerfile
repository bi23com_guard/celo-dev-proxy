 FROM node:12-alpine
 WORKDIR /celo-dev-proxy
 COPY . .
 CMD ["node", "index.js"]