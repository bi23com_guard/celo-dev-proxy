const http = require('http');
const https = require('https');

console.log(process.env)

const celoDevRPC = process.env['CELO_DEV_RPC']

function appendETHCompatible(rpcRequest, devCeloResponse, client){
    let result = devCeloResponse.result;

    if(null != result){
        if(rpcRequest.method == "eth_getBlockByNumber"){

            for(var i = 0; i < result.transactions.length; i++){

                if(!result.transactions[i].hasOwnProperty("ethCompatible")){

                    result.transactions[i].ethCompatible = true;
                }
                
            }

            if(result.epochSnarkData != null){

                result.epochSnarkData = null
            }
        }

        if(rpcRequest.method == "eth_getTransactionReceipt"){

            if(!result.transactions[i].hasOwnProperty("ethCompatible")){

                devCeloResponse.result.ethCompatible = true
            }
            
        }
    }

    let response = JSON.stringify(devCeloResponse);

    client.end(response);
}

function getRPCHost(url){

    let parts = url.split(':')
    let part = parts[1];
    let host = part.replace("//", "")

    return host;
}

function getRPCPort(url){

    let parts = url.split(':')
    let port = parts[2];

    return port;
}

function isHttps(url) {

    let parts = url.split(':')
    let protocol = parts[0]

    return protocol === "https";
}

function pingCeloDev(callback){
    console.log(`Waiting for celo node to be available (${celoDevRPC})`)

    let protocol = http;

    if (isHttps(celoDevRPC)){

        protocol = https;
    }
    
    protocol.get(celoDevRPC, (res)=>{

        callback()

    }).on("error", (error)=>{

        console.log(error)
    });
}

function toCeloDev(rpcRequest, client){


    let body = JSON.stringify(rpcRequest);

    let protocol = http;

    if (isHttps(celoDevRPC)){

        protocol = https;
    }

    let rpc = protocol.request({
        host: getRPCHost(celoDevRPC),
        port: getRPCPort(celoDevRPC),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(body)
        }
      }, (res) => {
        res.setEncoding('utf8');
        let buffer = ""
        res.on('data', (chunk) => {
            buffer += chunk
        });
        res.on('end', () => {
            try{
                let devCeloResponse = JSON.parse(buffer)
                console.log(buffer)
                appendETHCompatible(rpcRequest, devCeloResponse, client)
            }
            catch(error){
                client.end(buffer)
            }
        });
    });

    rpc.write(body);

    rpc.end();
}



const proxy = http.createServer((req, res) => {

    let buffer = ""
    req.on("data", (chunk)=>{

        buffer += chunk
    });

    req.on("end", ()=>{
        console.log(buffer)
        let request = null;
        try{
            request = JSON.parse(buffer);

        }catch(error){
            res.end("Not RPC Request Format!")
        }

        if(null != request){
            toCeloDev(request, res)
        }    

    });
});

proxy.on('clientError', (err, socket) => {
    socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});

let timer = setInterval(()=>{

    pingCeloDev(()=>{
        proxy.listen(9545);
        clearInterval(timer);
    })

}, 2000);